import os
import scipy.stats
import pandas

from bf import *

bf_dir = "../../bf/"

# feature set: # of decompiled bytes, longest consecutive bytes decompiled, decompiled result, # of opcode, identified opcode result, # of rung, # of membership of each n-gram, longest # of membership consecutively found, entropy 
class fs():
    def __init__(self, numOfDecompiled, decompiledRes, numOfOpcode, opcodeRes, numOfRung, numOfNgram, longestNgram, entropy):
        self.numOfDecompiled = numOfDecompiled
#        self.longestDecompiled = longestDecompiled
        self.decompiledRes = decompiledRes  # only for providing additional information (not used in training)

        self.numOfOpcode = numOfOpcode
        self.opcodeRes = opcodeRes          # only for providing additional information (not used in training)

        self.numOfRung = numOfRung

        self.numOfNgram = numOfNgram
        self.longestNgram = longestNgram

        self.entropy = entropy

"""
    count_ngram_membership
    ---------------
    - description: count membership for each n-gram (lower to upper) and longest # of membership consecutively found
    - input: data, lower n-gram, upper n-gram
    - output: # of membership of each n-gram (membership_cnt), longset # of membership (longestNgram) 
"""
def count_ngram_membership(data, lower_n, upper_n, bf_file_name_suffix):
    ngram_bf_list = []
    membership_cnt = []
    longestNgram = []
    consecutiveNgram = []
    
    for i in range(lower_n, upper_n+1):
        ngram_bf_list.append(bloomFilter())
        try:
            fileName = bf_dir + str(i) + bf_file_name_suffix
            ngram_bf_list[i-lower_n].open_bf(fileName)     
            # initialize the count and longest
            membership_cnt.append(0)    
            longestNgram.append(0)
            consecutiveNgram.append(0)
        except:
            print "Error: Can't open bloom fiter " + fileName
        
    for i in range(len(data)):
        for n in range(lower_n, upper_n+1):
            if len(data) - i >= n:
                if ngram_bf_list[n-lower_n].check_membership(data[i:i+n]):
                    membership_cnt[n-lower_n] += 1
                    consecutiveNgram[n-lower_n] += 1
                    if consecutiveNgram[n-lower_n] > longestNgram[n-lower_n]:
                        longestNgram[n-lower_n] = consecutiveNgram[n-lower_n]
                else:
                    consecutiveNgram[n-lower_n] = 0
 
    return membership_cnt, longestNgram                       


def calculate_entropy(data):
    dataToList = [data[i:i+1] for i in range(len(data))]
    p_data = pandas.Series(dataToList)
    
    return scipy.stats.entropy(p_data.value_counts(), base=2)


"""
    check_ngram_bf_files
    --------------------
    - description: check existance of legitimate file name for each n-gram bloom filter
    - input: (int) upper size of n-gram
    - output: verified result (True or Flase)
"""
def check_ngram_bf_files(lower_n, upper_n, bf_file_name_suffix):
    for i in range(lower_n, upper_n+1):
        fileName = bf_dir + str(i) + bf_file_name_suffix
        if os.path.isfile(fileName) == False:
            print "Error: Bloom filter file " + fileName + " is missing"
            return False
    return True


def writeColumnNames(fp, lower_n, upper_n):
    fp.write('pkt#,size,#dec,#op,#rung,')
    for i in range(lower_n, upper_n+1):
        fp.write('#%dgram,' % i)
    for i in range(lower_n, upper_n+1):
        fp.write('L%dgram,' % i)
    fp.write('entropy,class\n')


def writeFeatureValues(pktNum, size, fs, isCode, outFile, lower_n, upper_n):
#    outFile.write('%d,%d,%d,%d,%d,%d,' % (pktNum, size, fs.numOfDecompiled, fs.longestDecompiled, fs.numOfOpcode, fs.numOfRung))
    outFile.write('%d,%d,%d,%d,%d,' % (pktNum, size, fs.numOfDecompiled, fs.numOfOpcode, fs.numOfRung))
    
    for i in range(lower_n, upper_n+1):
        outFile.write('%d,' % fs.numOfNgram[i-lower_n])

    for i in range(lower_n, upper_n+1):
        outFile.write('%d,' % fs.longestNgram[i-lower_n])

    outFile.write('%.2f,%d\n' % (fs.entropy, isCode))


def print_fs(fs):
    print "* number of decompiled byte: ", fs.numOfDecompiled
#    print "* longest decompiled byte: ", fs.longestDecompiled
    print "* number of identified opcode: ", fs.numOfOpcode
    print "* number of identified rung: ", fs.numOfRung
    print "* membership counts: ", fs.numOfNgram
    print "* longest consecutive membership: ", fs.longestNgram
    print "* entropy: ", fs.entropy

    print "\n--opcodeRes--"
    print fs.opcodeRes

    print "\n--decompiledRes--"
    print fs.decompiledRes

    print "\n"
