import ConfigParser

conf = ConfigParser.ConfigParser()
conf.read('instructionsConfig.ini')

for s in conf.sections():
    conf.set(s, 'opcode', s[2:]+s[0:2])

with open('modified_conf.ini', 'w') as confFile:
    conf.write(confFile)
