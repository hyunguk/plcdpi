import sys, os
import argparse
from os import listdir
from os.path import isfile

sys.path.append("../")

from common import * 
from extract_pccc_wp import *
import rslogix 

BF_FILE_NAME_SUFFIX = "gram_pccc.bf"
#SHADOW_MEMORY_SCAN_BOUND = 46       # Longest length of instruction in ML1400 control logic: LCD Display 
#SHADOW_MEMORY_SCAN_BOUND = 80       # Maximum Payload length of the PCCC protocol
SHADOW_MEMORY_SCAN_BOUND = 80

class shadow_memory():
    def __init__(self):
        self.mem = {}

    def write_data(self, file_num, file_type, element_num, addr, size, data):
        if (file_num, file_type, element_num) not in self.mem:
            self.mem[(file_num, file_type, element_num)] = bytearray('\x00') * 65536  # sub-element field is 2 bytes

        self.mem[(file_num, file_type, element_num)][addr:addr+size] = data    


    def read_data(self, file_num, file_type, element_num, lower_addr, upper_addr):
        if lower_addr < 0:
            lower_addr = 0

        if upper_addr > 65535:
            upper_addr = 65535

        return self.mem[(file_num, file_type, element_num)][lower_addr:upper_addr]

"""
    extrat_features
    ---------------
    - description: extract features from data (data can be either write payload or part of shadow memory)
    - input: data, lower n-gram, upper n-gram
    - output: a fs (feature set) object
"""
def extract_features(data, lower_n, upper_n):
    # n-gram
    numOfNgram, longsetNgram = count_ngram_membership(data, lower_n, upper_n, BF_FILE_NAME_SUFFIX)

    # entropy
    entropy = calculate_entropy(data)

    # opcode identification
    opcode_list = rslogix.identify_opcode(data)
    opcode_cnt_all = 0
    for opcode in opcode_list:
        opcode_cnt_all += opcode.cnt

    # rung identification
    rung_list = rslogix.identify_rung(data)
    numOfRung = 0
    for rung in rung_list:
        #print "rung: %s, %d" % (binascii.hexlify(rung.rung), len(rung.rung))
        if rung.complete:
            numOfRung += 1

    # decompilation
    decompiledRes, numOfDecompiled = rslogix.decompile(data)
    #print "* decomp: ", decompiledBytes

    #return fs(0, "", 0, [], 0, numOfNgram, longsetNgram, 0)   
    return fs(numOfDecompiled, decompiledRes, opcode_cnt_all, opcode_list, numOfRung, numOfNgram, longsetNgram, entropy)   


def main():
    parser = argparse.ArgumentParser(description="Feature extractor for PCCC write packet payload")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-p", "--packet", help="packet basis extraction", action="store_true")
    group.add_argument("-m", "--shadow-memory", help="shadow memory basis extraction", action="store_true")
    parser.add_argument("-v", "--verbose", help="display additional information on screen", action="store_true")
    parser.add_argument("-l", "--lower-ngram", help="lower n-gram filter", required=True)
    parser.add_argument("-u", "--upper-ngram", help="2 to n gram bloom filters are used (each filter file is needed which can be generated using pccc_bf_gen.py)", required=True)
    parser.add_argument("pcap_directory", help="directory containing pcap files")

    args = parser.parse_args()

    if check_ngram_bf_files(int(args.lower_ngram), int(args.upper_ngram), BF_FILE_NAME_SUFFIX) == False:
        sys.exit()

    if args.packet:
        print "Packet basis feature extraction"
    elif args.shadow_memory:
        print "Shadow Memory basis feature extraction"
    
    pcap_files = [f for f in listdir(args.pcap_directory) if isfile(os.path.join(args.pcap_directory,f)) and "pcap" in f.split(".")[-1]]
    
    outDirPath = "./pccc_data/"

    if args.shadow_memory:
        #mem = shadow_memory()  # shadow memory is not reset
        print "scan boundary size: ", SHADOW_MEMORY_SCAN_BOUND

    for f in pcap_files:
        filePath = os.path.join(args.pcap_directory,f)
        if args.packet:
            subName = "packet"
        elif args.shadow_memory:
            subName = "shadow"
            mem = shadow_memory()  # shadow memory is reset for every pcap

        dirName = args.pcap_directory.split("/")[-1] if args.pcap_directory.split("/")[-1] != "" else args.pcap_directory.split("/")[-2]
        outFilePath = os.path.join(outDirPath, dirName, subName, f.split('.')[0]+"_"+subName+".data")
        if not os.path.exists(os.path.dirname(outFilePath)):
            try:
                os.makedirs(os.path.dirname(outFilePath))
            except:
                print "Error. failed to make dir: " + os.path.dirname(outFilePath)

        outFile = open(outFilePath, "w")

        # write names of column
        writeColumnNames(outFile, int(args.lower_ngram), int(args.upper_ngram))

        print "Processing " + filePath + " ............"

        wp_list = extract_pccc_wp(filePath)       

        for wp in wp_list:
            if args.verbose:
                print "* pkt [", wp.pktNum, "]"
                print "* file num: " + binascii.hexlify(wp.file_num)
                print "* file type: " + binascii.hexlify(wp.file_type)
                print "* element num: ", wp.element_num
                print "* sub-element num: ", wp.subelement_num
                print "* size: ", wp.size
                print "* payload: " + binascii.hexlify(wp.payload)
                print "* code block: ", wp.isCode

            # packet basis extraction
            if args.packet:
                fs = extract_features(wp.payload, int(args.lower_ngram), int(args.upper_ngram))

                # write features to file
                writeFeatureValues(wp.pktNum, wp.size, fs, wp.isCode, outFile, int(args.lower_ngram), int(args.upper_ngram))

            # shadow memory basis extraction
            elif args.shadow_memory:
                addr = wp.subelement_num * 2
                mem.write_data(wp.file_num, wp.file_type, wp.element_num, addr, wp.size, wp.payload)
                memScanData = bytes(mem.read_data(wp.file_num, wp.file_type, wp.element_num, addr - SHADOW_MEMORY_SCAN_BOUND, addr + wp.size + SHADOW_MEMORY_SCAN_BOUND))
                fs = extract_features(memScanData, int(args.lower_ngram), int(args.upper_ngram))

                # write features to file
                writeFeatureValues(wp.pktNum, len(memScanData), fs, wp.isCode, outFile, int(args.lower_ngram), int(args.upper_ngram))

                # Reset the area of shadow memory written
                """
                if wp.isCode:
                    mem.write_data(wp.file_num, wp.file_type, wp.element_num, addr, wp.size, '\x00'*wp.size)
                """

                if args.verbose:
                    print "* memory scan (%d): %s" % ( len(memScanData), binascii.hexlify(memScanData))           

            # stdout 
            if args.verbose:
                print_fs(fs)


        outFile.close()

    """
    if args.shadow_memory:
        print "Total size of shadow memory: ", len(mem.mem)
    """

if __name__ == '__main__':
    main()
