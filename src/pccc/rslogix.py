import ConfigParser
import sys
import binascii
import struct

from extract_pccc_wp import *

conf_file_name = 'instructionsConfig.ini'
END_OPCODE = '\x30\x00'
MIN_RUNG_SIZE = 8  # 2 : 0x0000, 2 : rung signature, 2 : rung size, 2 : min leng of instruction (END: 0x3000)
MAX_RUNG_SIZE = 5000  # decided heuristically. likely a rung would not exceed this for real-world rslogix control logix


try:
    conf = ConfigParser.ConfigParser()
    conf.read(conf_file_name)

except:
    print "Error. Can't read rslogix instruction conf file: ", conf_file_name
    sys.exit()


branch_inst = ['0800', 'd002', '1000', '1400', '0c00', 'd402']


class opcode_cnt():
    def __init__(self, opcode, inscode, cnt):
        self.opcode = opcode
        self.inscode = inscode
        self.cnt = cnt
    def __repr__(self):
        return "(%s, %s, %d)" % (self.opcode, self.inscode, self.cnt)

class Rung():
    def __init__(self, rung, complete):
        self.rung = rung
        self.complete = complete  # is rung complete? true / false        

def identify_opcode(data):
    data = binascii.hexlify(data)
    opcode_list = []

    for s in conf.sections():
        opcode = conf.get(s, 'opcode').lower()
        inscode = conf.get(s, 'inscode')
        cnt = data.count(opcode)
        if cnt > 0:
            opcode_list.append(opcode_cnt(opcode, inscode, cnt))
    
    return opcode_list


def identify_rung(data):
    rung_list = []
    
    i=0

    endOffPreRung = 0    # end offset of previous rung

    while i <= len(data)-6:       # To check a rung, it needs size of rung field. (2: 0000, 2: rung signature, 2: rung size)
        if data[i:i+2] == '\x00\x00':  # start of rung
            rung_size = struct.unpack("<H", data[i+4:i+6])[0]
            if rung_size >= MIN_RUNG_SIZE and rung_size <= MAX_RUNG_SIZE:   
                if i+rung_size > len(data):     # we assume this is a fragmented rung (not complete rung)
                    rung_list.append(Rung(data[endOffPreRung:i], False)) # for non-rung data beteen rung or the very first parts of the data before the first rung
                    rung_list.append(Rung(data[i:], False))
                    endOffPreRung = len(data)
                    i = len(data)
                elif i+rung_size == len(data): # we assume this is a complete rung
                    rung_list.append(Rung(data[endOffPreRung:i], False))
                    rung_list.append(Rung(data[i:], True))
                    endOffPreRung = len(data)
                    i = len(data)
                elif i+rung_size == len(data)-1:
                    if data[i+rung_size:i+rung_size+1] != '\x00':    # not a rung
                        i += 1
                    else:
                        rung_list.append(Rung(data[endOffPreRung:i], False))
                        rung_list.append(Rung(data[i:i+rung_size], True))
                        endOffPreRung = i+rung_size
                        i += rung_size
                else:
                    if data[i+rung_size:i+rung_size+2] != '\x00\x00':   # not a rung                                
                        i += 1
                    else:
                        rung_list.append(Rung(data[endOffPreRung:i], False))
                        rung_list.append(Rung(data[i:i+rung_size], True))
                        endOffPreRung = i+rung_size
                        i += rung_size
            else:       
                i += 1
        else:       # not a '0000': it is not a rung candidate
            i += 1

    if endOffPreRung < len(data):
        rung_list.append(Rung(data[endOffPreRung:], False))
    
    rung_list = [rung for rung in rung_list if rung.rung != '']

    return rung_list


def getBytesInfo(target):
    target = binascii.hexlify(target)

    if target == '0000':
        return ("Rung", "(Rung)", 6)

    if target in branch_inst:
        return ("Opcode", "br", 2)

    for s in conf.sections():
        if target == conf.get(s, 'opcode').lower():
            return ("Opcode", conf.get(s, 'inscode'), conf.getint(s, 'size'))

    return None


def verifyNextTarget(cur_instype, offset_next, nextTarget_info, data):
    if nextTarget_info is None:
        return False

    next_instype = nextTarget_info[0]

    # legitimate combination: 1) opcode + opcode, 2) opcode + rung, 3) rung + opcode, 4) END
    if cur_instype == "Rung" and next_instype == "Rung":
        return False

    if next_instype == "Rung":
        if offset_next+6 > len(data):   # we can't verify the size field of the rung
            return False
        else:
            rung_size = struct.unpack("<H", data[offset_next+4:offset_next+6])[0] 
            if rung_size < MIN_RUNG_SIZE or rung_size > MAX_RUNG_SIZE:   
                return False
    return True    
        
def verifyOperand(inscode, bytecodes):
    if inscode == "CLR":
        if bytecodes[2:4] != '\x0a\x00' or bytecodes[8:10] != '\x00\x00':
            return False
    if inscode == "TOF":
        if bytecodes[2:4] != '\x0a\x00' or struct.unpack("<H", bytecodes[8:10])[0] > 15:
            return False
    if inscode == "LES":
        if bytecodes[2:4] != '\x10\x00':
            return False
    if inscode == "RTO":
        if bytecodes[2:4] != '\x0a\x00':
            return False
    if inscode == "AND":
        if bytecodes[2:4] != '\x16\x00':
            return False
    if inscode == "SUB":
        if bytecodes[2:4] != '\x16\x00':
            return False
    if inscode == "EQU":
        if bytecodes[2:4] != '\x10\x00':
            return False

    return True

"""
    decompileRange()
    ---------------
    - description: find decompilerable range 
    - input: data
    - output: start and end offset over which data can be decompiled, decompiled result
"""
def decompileRange(data):
    dataLength = len(data)
    longest_decompiled = 0
    longest_start = 0
    longest_end = 0
    longest_res = ''

    for start in range(dataLength-1):
        target = data[start:start+2]
        target_info = getBytesInfo(target)        
 
        valid = False
   
        if target_info is not None:

            instype = target_info[0]
            inscode = target_info[1]
            size = target_info[2]
            res = ''
            numOfDecompiled = 0

            nextOffset = start + size

            while nextOffset < dataLength-1:
                if verifyOperand(inscode, data[nextOffset-size:nextOffset]) == False:
                    break
                
                nextTarget = data[nextOffset:nextOffset+2]
                nextTarget_info = getBytesInfo(nextTarget)

                if verifyNextTarget(instype, nextOffset, nextTarget_info, data) == True:
                    res += " " + inscode + " " + binascii.hexlify(data[nextOffset-size+2:nextOffset])
                    numOfDecompiled += size

                    instype = nextTarget_info[0]
                    inscode = nextTarget_info[1]
                    size = nextTarget_info[2]

                    nextOffset = nextOffset + size

                    if inscode == 'END':
                        res += " " + inscode
                        break
                else:
                    break
 
            if numOfDecompiled > longest_decompiled:
                longest_decompiled = numOfDecompiled
                longest_start = start
                if inscode == 'END':
                    longest_end = nextOffset
                else:
                    longest_end = nextOffset - size
                longest_res = res
    
    return longest_decompiled, longest_start, longest_end, longest_res


def decompile(data):
    numOfDecompiled, start, end, res = decompileRange(data)

    if numOfDecompiled != 0:
        decompiled_res = binascii.hexlify(data[:start]) + " [ " + res + " ] " + binascii.hexlify(data[end:]) 
        #decompiled_bytes = binascii.hexlify(data[:start]) + ("x" * (len(data[start:end])*2)) + binascii.hexlify(data[end:])
    else:
        decompiled_res = ''
        #decompiled_bytes = ''
    
    return decompiled_res, numOfDecompiled

def main():
    wp_list = extract_pccc_wp(sys.argv[1])

    for wp in wp_list:
        print "* pkt [", wp.pktNum, "]"
        print "* file num: " + binascii.hexlify(wp.file_num)
        print "* file type: " + binascii.hexlify(wp.file_type)
        print "* element num: ", wp.element_num
        print "* sub-element num: ", wp.subelement_num
        print "* size: ", wp.size
        print "* payload: " + binascii.hexlify(wp.payload)
        print "* code block: ", wp.isCode

        opcode_list = identify_opcode(wp.payload)
        opcode_cnt_all = 0
        print opcode_list
        for opcode in opcode_list:
            opcode_cnt_all += opcode.cnt

        print "* opcode count: ", opcode_cnt_all
        
        rung_list = identify_rung(wp.payload)
        for rung in rung_list:
            print "rung: %s, %d" % (binascii.hexlify(rung.rung), len(rung.rung))
            print rung.complete

        decompiled_res, decompiled_temp = decompile(wp.payload)
        print "decompiled"
        print "-----------"
        print decompiled_res
        print decompiled_temp

if __name__=='__main__':
    main()
