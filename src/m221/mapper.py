####################################
##Original Author:  Sushma Kalle  ##
##@Input:  Ladder Logic           ##
##@Output: Instruction List       ##
####################################

import pyshark
import sys
import os,errno
import numpy as np
import re
import MySQLdb
import memwords
import binascii
from operator import itemgetter



start = 0
flag = 0
thisline = ''
db = MySQLdb.connect(host="localhost",  # host 
                     user="root",       # username
                     passwd="1234",    # password
                     db="modicon")      # name of the database

cur = db.cursor()
cur.execute("select op_code,instruction,output from instructions ORDER BY CHAR_LENGTH(op_code) DESC")
rows = cur.fetchall()
cur.execute("select op_code,delimiter,category from delimiters")
delimiters = cur.fetchall()
cur.execute("select operation from operations")
operations = cur.fetchall()
cur.execute("select op_code,instruction,output from instructions where output=\'0\' ORDER BY CHAR_LENGTH(op_code) DESC")
inputs = cur.fetchall()

# Replace function for repeated occurances
def rreplace(s, old, new, occurrence):
    li = s.rsplit(old, occurrence)
    return new.join(li)

# End of rung condtion: 1. output followed by input (only output doesn't mean end of rung), 2. end_blk 
# Rung always starts with input instruction or start block (7f1a)

# block: operation block(start: 7f1axx) / conditional block (start: 7f1a10)

# Returns Rung end addresses
def llToRungs(ll,rows,delimiters,inputs):
    ll      = ll[:-2]
    ends    = []
    rungs   = []
    end_l   = start_l   = ins_e = ''
    cut_iter= 0
    input_addresses = []

    rungs_temp = []

    inputs = list(inputs)
    inputs.append('7f1a')

    # To get the addresses of every end instruction
    # rows: all instruction from instructions table
    # delimiters: all delimiters from delimiters table
    # inputs: only input instructions from the instructions table
    for row in rows:
        temp = []
        # row[0]: instruction, row[2] == 1 is output
        if row[0] in ll and row[2] == 1:
            temp = [m.start() for m in re.finditer(row[0],ll)]
            for te in temp:
                # ends: every offset and length of all the output instructions in the payload
                ends.append((te,len(row[0])))

    # sorted by its address (offset) of output
    ends = sorted(ends, key=lambda ends: ends[0])
    
    # 7f1a: start of operation block 
    
    # The rungs with block instruction has a delimiter at the end =>  7f1a10: start, 7f1a11: out_blk / end_blk (we should end the rung at end_blk)
    for de in delimiters:
        if de[2] == 1:      # end_blk 7f1a11
            end_blk = [m.start() for m in re.finditer(de[0],ll)]
            end_l = de[0]   
            ins_e = de[1]
        if de[2] == 0:      # start_blk 7f1a10
            start_l = de[1]
    end_blk = end_blk[1::2] # to get only end_blk not out_blk
    # Getting the addresses of Input instructions
    for ip in inputs:
        if ip[0] in ll:
            temp = [m.start() for m in re.finditer(ip[0],ll)]
            for tem in temp:
                input_addresses.append(tem)
    
    cut = 0

    #Getting the rung end addresses from the control logic 
    for end, length in ends:
        if end+length in end_blk:   # output followed by end_blk
            cut = end + length + len(end_l)
        elif end+length in input_addresses: # output followed by input
            cut = end+length
        rung = rreplace(ll[:cut-cut_iter], end_l , "\n" +ins_e, 1)
        rung_temp = rreplace(ll[:cut-cut_iter], end_l , "x"*len(end_l), 1)

        rungs.append(rung)
        rungs_temp.append(rung_temp)
    
        ll = ll[cut-cut_iter:]
        cut_iter = cut
    rungs.append(ll)
    rungs_temp.append(ll)

    return rungs,start_l,rungs_temp
        


# Decompiling Operational Block
def memOperationRung(rung,op):
    global thisline
    global flag
    length = ''
    try:
        length = rung[rung.find(op)-2:rung.find(op)]    # length of one operation in operation block
        if rung[rung.find(op)-6:rung.find(op)-4] == '03' or flag == 1:  # flag == 1: multiple lines of operations in operation block (means multiple 7f1axx)
            flag = 1
            offset = rung.find(op)+int(length,16)*2 - 4 
            thisline = thisline + rung[rung.find(op)-4:offset]

            if rung[offset:offset+2] == '03' or not rung.find('7f1a',offset):
                return thisline
            else:       # there is another operation following
                n_op =rung.find('7f1a',offset)
                return memOperationRung(rung[n_op-6:],rung[n_op:n_op+6])
        else:
            
            offset = rung.find(op)+int(length,16)*2 -4
            thisline = rung[rung.find(op)-4:offset]
            return thisline
    except:
#       continue
        pass


def map(ll):
    global cur
    returnIns = ''
    result = ''
    #ll = extractor_refined.ladder_logic
    #ll = ll[24:].lower().replace(":","")

    
    rungs,start_l,rungs_temp = llToRungs(ll,rows,delimiters,inputs)
    rungs_temp = [rung_temp for rung_temp in rungs_temp if rung_temp != '']

    # Decompiling one rung at a item
    rungNum = 0
    for rung in rungs:
        global line
        global flag
        global thisline  
        blk_t = 0
        if rung != '':  
            j = 0
            if_OP = 100000
            first_op = ''
            try:
                for op in operations:   # to get the very first operation
                    if op[0] in rung:
                        op_pos = rung.find(op[0])
                        if if_OP > op_pos:
                            if_OP = op_pos
                            first_op = op[0]
                st = memOperationRung(rung,first_op)
                flag = 0
                thisline = ''
                rep = memwords.parse(st)
                try:
                    if rung[rung.find(first_op)-8:rung.find(first_op)-4] == '0303':
                        rep = "OPER "+rep
                        which = r'....'+st+'..'
                        replaced_num = 4 + len(st) + 2
                    elif rung[rung.find(first_op)-8:rung.find(first_op)-4] == '03':
                        rep = "AND "+rep
                        which = r'..'+st+'..'
                        replaced_num = 2 + len(st) + 2
                    else:
                        which = r''+st
                        replaced_num = len(st)
                    rung = re.sub(which, rep + '\n', rung)
                    rungs_temp[rungNum] = re.sub(which, "x"*replaced_num, rungs_temp[rungNum])
                except:
                    pass
            except:
                pass
            for row in rows:                
                if row[0] in rung and row[2] ==1:   # output 
                    rung = rung.replace(row[0], row[1] )
                    rungs_temp[rungNum] = rungs_temp[rungNum].replace(row[0], "x"*len(row[0]))
                if row[0] in rung and row[2] == 0:  # input
                    rung = rung.replace(row[0],row[1]+"\n")
                    rungs_temp[rungNum] = rungs_temp[rungNum].replace(row[0], "x"*len(row[0]))
                if row[0] in rung and row[2] == 2:  # AND, OR
                    rung = re.sub(r''+row[0]+'..', row[1], rung)
                    rungs_temp[rungNum] = re.sub(r''+row[0]+'..', "x"*(len(row[0])+2), rungs_temp[rungNum])
                if row[0] in rung and row[2] == 3:  # AND (, )
                    rung = rung.replace(row[0], row[1])
                    rungs_temp[rungNum] = rungs_temp[rungNum].replace(row[0], "x"*len(row[0]))
            ins = rung.replace("fc",'').replace("03",'').replace("7e5bfbe604",'').split("\n")
            rungs_temp[rungNum] = rungs_temp[rungNum].replace("xx03xx","xxxxxx").replace("xxfcxx","xxxxxx").replace("xx7e5bfbe604xx","xxxxxxxxxxxxxx").replace("xx7f1a11xx","xxxxxxxxxx")
            
            returnIns = returnIns + "\n" + rung
            if start_l in ins[0]:
                try:
                    ins[1] = "LD" + ins[1]
                except:
                    pass
            else:
                ins[0] = "LD" + ins[0]
            for inst in ins: 
                result = result +  "%04d  | " % j + inst +"\n"
                j = j+1
            result = result + "---------------------------"  + "\n"

            rungNum += 1
            
    result = result + "*****************************************"
    return result, rungs_temp

def main():
    codeFile = open(sys.argv[1], 'r')
    code = codeFile.read()
    
    res, res_temp = map(binascii.hexlify(code))
    print res

if __name__ == '__main__':
    main()
