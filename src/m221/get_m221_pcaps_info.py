import os
from scapy.all import *
import argparse
from os import listdir
from os.path import isfile

# transaction id(2) + protocol identifier(2) + length(2) + unit identifier(1) + function code(1)
MODBUS_HDR_LEN = 8

def get_m221_pkts_info(pcapFile):
    all_pkts = rdpcap(pcapFile)

    pktNum = 0
    m221_pkts_num = 0
    write_req_num = 0
    code_pkt_num = 0

    modbus_msgs = []
    for pkt in all_pkts:
        pktNum += 1

        # we are only interested in request packets
        if TCP in pkt and (pkt[TCP].dport == 502 or pkt[TCP].sport == 502) and len(pkt[TCP].payload) > MODBUS_HDR_LEN:
            modbus_msgs.append(bytes(pkt[TCP].payload))


    for modbus in modbus_msgs:
        if modbus[7] != '\x5a':    # modbus function code "0x5a" indicates that modbus data contains Schneider M221 protocol message
            continue
        
        m221_pkts_num += 1        

        m221_msg = modbus[MODBUS_HDR_LEN:]
        
        # M221 write request
        if m221_msg[1] == '\x29':
            write_req_num += 1

            addr = struct.unpack("<H", m221_msg[2:4])[0]
            addr_type = m221_msg[4:6]
            data_size = struct.unpack("<H", m221_msg[6:8])[0]
            data = m221_msg[8:]

            # get ground truth based on address
            # code block starts above 0xe000 and conf1 block starts from 0xfed4 
            if addr >= 0xe000 and addr < 0xfed4 and addr_type == '\x01\x07':
                code_pkt_num += 1

    return m221_pkts_num, write_req_num, code_pkt_num


def main():
    parser = argparse.ArgumentParser(description="Get basic information from the M221 pcap files")
    parser.add_argument("pcap_directory", help="directory containing pcap files")

    args = parser.parse_args()

    pcap_files = [f for f in listdir(args.pcap_directory) if isfile(os.path.join(args.pcap_directory,f)) and "pcap" in f.split(".")[-1]]

    total_pcaps = 0
    total_m221_pkts = 0
    total_write_req = 0
    total_code_pkts = 0

    for f in pcap_files:
        total_pcaps += 1

        filePath = os.path.join(args.pcap_directory,f)
        print "File: " + filePath    

        m221_pkts_num, write_req_num, code_pkt_num = get_m221_pkts_info(filePath)
        total_m221_pkts += m221_pkts_num
        total_write_req += write_req_num
        total_code_pkts += code_pkt_num

        print total_m221_pkts, total_write_req, total_code_pkts

    print "\nTotal # of pcap files: ", len(pcap_files)
    print "Total # of m221 packets: ", total_m221_pkts
    print "Total # of write request: ", total_write_req
    print "Total # of packet containg code: ", total_code_pkts

if __name__ == '__main__':
    main()
