from scapy.all import *

# transaction id(2) + protocol identifier(2) + length(2) + unit identifier(1) + function code(1)
MODBUS_HDR_LEN = 8

# write payload: pkt number, address, size, (write) payload, isCode (is it code block?)
class wp():
    def __init__(self, pktNum, addr, addr_type, size, payload, isCode):
        self.pktNum = pktNum
        self.addr = addr
        self.addr_type = addr_type
        self.size = size
        self.payload = payload
        self.isCode = isCode
"""
    extract_m221_wp
    - description: extract write payload of M221 protocol
    - input: pcap file
    - output: a list of wp objects
"""
def extract_m221_wp(pcapFile):
    all_pkts = rdpcap(pcapFile)

    pktNum = 0
    modbus_reqs = []
    for pkt in all_pkts:
        pktNum += 1

        # we are only interested in request packets
        if TCP in pkt and pkt[TCP].dport == 502 and len(pkt[TCP].payload) > MODBUS_HDR_LEN:
            modbus_reqs.append((pktNum, bytes(pkt[TCP].payload)))   

    wp_list = []

    for modbus_req in modbus_reqs:
        pktNum = modbus_req[0]
        payload = modbus_req[1]
        if payload[7] != '\x5a':    # modbus function code "0x5a" indicates that modbus data contains Schneider M221 protocol message
            continue
        
        m221_msg = payload[MODBUS_HDR_LEN:]
        
        # M221 write request
        if m221_msg[1] == '\x29':
            addr = struct.unpack("<H", m221_msg[2:4])[0]
            addr_type = m221_msg[4:6]
            data_size = struct.unpack("<H", m221_msg[6:8])[0]
            data = m221_msg[8:]

            # get ground truth based on address
            # code block starts above 0xe000 and conf1 block starts from 0xfed4 
            if addr >= 0xe000 and addr < 0xfed4 and addr_type == '\x01\x07':
                isCode = True
            else:
                isCode = False

            wp_list.append(wp(pktNum, addr, addr_type, data_size, data, isCode))

    return wp_list
