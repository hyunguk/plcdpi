import sys, os
import argparse
import binascii
from os import listdir
from os.path import isfile

sys.path.append("../")

import mapper
import opcode_id
from common import * 
from extract_m221_wp import *


BF_FILE_NAME_SUFFIX = "gram_m221.bf"
MAX_M221_PAYLOAD_SIZE = 236
SHADOW_MEMORY_SCAN_BOUND = 236
#SHADOW_MEMORY_SCAN_BOUND = 11       # maximum size of instruction

TEMP_ISCODE = False

class shadow_memory():
    def __init__(self):
        self.mem = {}

    def write_data(self, addr_type, addr, size, data):
        if addr_type not in self.mem:
            self.mem[addr_type] = bytearray('\x00') * 65536   # M221 addr space 0 ~ 0xffff

        self.mem[addr_type][addr:addr+size] = data    

    def read_data(self, addr_type, lower_addr, upper_addr):
        if lower_addr < 0:
            lower_addr = 0

        if upper_addr > 65535:
            upper_addr = 65535

        return self.mem[addr_type][lower_addr:upper_addr]

"""
    extrat_features
    ---------------
    - description: extract features from data (data can be either write payload or part of shadow memory)
    - input: data, lower n-gram, upper n-gram
    - output: a fs (feature set) object
"""
def extract_features(data, lower_n, upper_n):
    # n-gram
    numOfNgram, longsetNgram = count_ngram_membership(data, lower_n, upper_n, BF_FILE_NAME_SUFFIX)

    # entropy
    entropy = calculate_entropy(data)

    # decompilation, rung identification
    decompiledRes, rungs_temp = mapper.map(binascii.hexlify(data))
    numOfRung = len(rungs_temp)

    numOfDecompiled = 0
    longestDecompiled = 0

    
    decompiledPayload = ""

    for rung in rungs_temp:
        decompiledPayload += rung

    #print "=decompiledPayload: " + decompiledPayload
    tempDecompiled = 0
    

    for byte in decompiledPayload:
        if byte == "x":
            numOfDecompiled += 1
            tempDecompiled += 1
            if tempDecompiled > longestDecompiled:
                longestDecompiled = tempDecompiled

        else:
            tempDecompiled = 0

    numOfDecompiled = numOfDecompiled / 2
    longestDecompiled = longestDecompiled / 2

    # opcode identification
    opcodeResList = opcode_id.findOpcodes(binascii.hexlify(data))
    opcodeRes = ""
    for opcode in opcodeResList:
        opcodeRes += opcode
 
    # rungs_temp contains one rung as default value. So, we need to consider this.           
    if numOfRung == 1:
        # A rung should contain at least 5 bytes and two opcode. 
        if numOfDecompiled < 5 or len(opcodeResList) < 2:
            numOfRung = 0

#    return fs(0, "", 0, "", 0, numOfNgram, longsetNgram, 0)    # used for overhead test with n-gram 
    return fs(longestDecompiled, decompiledRes, len(opcodeResList), opcodeRes, numOfRung, numOfNgram, longsetNgram, entropy)   


def main():
    parser = argparse.ArgumentParser(description="Extract features from the payload of the M221 write request messages")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-p", "--packet", help="packet-basis extraction", action="store_true")
    group.add_argument("-m", "--shadow-memory", help="shadow memory-basis extraction", action="store_true")
    parser.add_argument("-v", "--verbose", help="verbose mode", action="store_true")
    parser.add_argument("-l", "--lower-ngram", help="lower bound of n-gram filter", required=True)
    parser.add_argument("-u", "--upper-ngram", help="upper bound of n-gram filter", required=True)
    parser.add_argument("pcap_directory", help="directory containing the pcap files")

    args = parser.parse_args()

    if check_ngram_bf_files(int(args.lower_ngram), int(args.upper_ngram), BF_FILE_NAME_SUFFIX) == False:
        sys.exit()

    if args.packet:
        print "Packet-basis feature extraction"
    elif args.shadow_memory:
        print "Shadow Memory-basis feature extraction"
    
    pcap_files = [f for f in listdir(args.pcap_directory) if isfile(os.path.join(args.pcap_directory,f)) and "pcap" in f.split(".")[-1]]
    
    outDirPath = "./m221_data/"

    if args.shadow_memory:
        #mem = shadow_memory()  # shadow memory is not reset
        print "scan boundary size: ", SHADOW_MEMORY_SCAN_BOUND

    for f in pcap_files:
        filePath = os.path.join(args.pcap_directory,f)
        if args.packet:
            subName = "packet"
        elif args.shadow_memory:
            subName = "shadow"
            mem = shadow_memory()  # shadow memory is reset at every pcap

        dirName = args.pcap_directory.split("/")[-1] if args.pcap_directory.split("/")[-1] != "" else args.pcap_directory.split("/")[-2]
        outFilePath = os.path.join(outDirPath, dirName, subName, f.split('.')[0]+"_"+subName+".data")
        if not os.path.exists(os.path.dirname(outFilePath)):
            try:
                os.makedirs(os.path.dirname(outFilePath))
            except:
                print "Error. failed to make dir: " + os.path.dirname(outFilePath)

        outFile = open(outFilePath, "w")

        # write names of column
        writeColumnNames(outFile, int(args.lower_ngram), int(args.upper_ngram))

        print "Processing " + filePath + " ............"

        wp_list = extract_m221_wp(filePath)       

        for wp in wp_list:
            if args.verbose:
                print "* pkt [", wp.pktNum, "]"
                print "* addr: " + hex(wp.addr)
                print "* addr type: " + binascii.hexlify(wp.addr_type)
                print "* size: ", wp.size
                print "* payload: " + binascii.hexlify(wp.payload)
                print "* code block: ", wp.isCode

            # packet basis extraction
            if args.packet:
                fs = extract_features(wp.payload, int(args.lower_ngram), int(args.upper_ngram))

                # write features to file
                writeFeatureValues(wp.pktNum, wp.size, fs, wp.isCode, outFile, int(args.lower_ngram), int(args.upper_ngram))

            # shadow memory basis extraction
            elif args.shadow_memory:
                mem.write_data(wp.addr_type, wp.addr, wp.size, wp.payload)
                memScanData = bytes(mem.read_data(wp.addr_type, wp.addr - SHADOW_MEMORY_SCAN_BOUND, wp.addr + wp.size + SHADOW_MEMORY_SCAN_BOUND))
                fs = extract_features(memScanData, int(args.lower_ngram), int(args.upper_ngram))

                # write features to file
                writeFeatureValues(wp.pktNum, len(memScanData), fs, wp.isCode, outFile, int(args.lower_ngram), int(args.upper_ngram))
                """
                # Reset the area of shadow memory written
                if wp.isCode:
                    mem.write_data(wp.addr, wp.size, '\x00'*wp.size)
                """

                if args.verbose:
                    print "* memory scan (%d): %s" % ( len(memScanData), binascii.hexlify(memScanData))           

            # stdout 
            if args.verbose:
                print_fs(fs)


        outFile.close()
    """
    if args.shadow_memory:
        print "Total size of shadow memory: ", len(mem.mem)
    """

if __name__ == '__main__':
    main()
