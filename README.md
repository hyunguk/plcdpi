Shade v1.0
================

Shade detects PLC control logic code in the network traffic. Currently, it supports Schneider Electric's Modicon M221 PLC and Allen-Bradley's MicroLogix 1400 PLC.

Requirements
-------------
* python 2.7
* pyshark: pip install pyshark-legacy
* scikit-learn: pip install scikit-learn
* scipy: pip install scipy
* numpy: pip install numpy
* pandas: pip install pandas
* bloom filter https://github.com/axiak/pybloomfiltermmap
* scapy https://github.com/secdev/scapy
* matplotlib: sudo apt install python-matplotlib
* MySQL-python: sudo apt install python-mysqldb
* mysql-server: sudo apt install mysql-server
* Import a database for m221 decompiler: mysql -u root -p modicon < m221_eupheus_db.sql

Main Modules
-------------
* M221 feature extractor: src/m221/m221_feature_extractor.py
* ML1400 feature extractor: src/pccc/pccc_feature_extractor.py
* Classifier: src/classifier.py


Paper
------------
Hyunguk Yoo, Sushma Kalle, Jared Smith, Irfan Ahmed, Overshadow PLC to Detect Remote Control-Logic Injection Attacks, DIMVA 2019



